package try_catch;

import java.lang.ArrayIndexOutOfBoundsException;
import java.util.Scanner;

public class Task1 {
	public static void main(String[] args) {
		String newTab[] = new String[2];
		String newInput;
		int tmpInt = 0;
		Scanner sc = new Scanner(System.in);

		try {
			for(int i = 0; i < newTab.length+1; i++) {
				System.out.println("dodajemy do tablicy: ");
				newTab[i] = sc.next();
			}
		}
		catch(ArrayIndexOutOfBoundsException e) {
			System.out.println(e);
			System.out.println("wyswietl to!");
			tmpInt++;
		}
		finally {
			System.out.println("wysralo sie :P");
			System.out.println(tmpInt);
		}
		sc.close();
	}
}
