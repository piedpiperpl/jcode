package try_catch;

import java.util.InputMismatchException;
import java.util.Scanner;

public class Task2 {
	public static void main(String[] args) {
		int newTab[] = new int[2];
		int newInput;
		int tmpInt = 0;
		Scanner sc = new Scanner(System.in);

		try {
			for(int i = 0; i < newTab.length; i++) {
				System.out.println("dodajemy liczby: ");
				newTab[i] = sc.nextInt();
			}
		}
		catch(InputMismatchException e) {
			System.out.println(e);
			System.out.println("wyswietl to!");
			tmpInt++;
		}
		finally {
			System.out.println("wysralo sie :P");
			System.out.println(tmpInt);
		}
	}
}

